'use strict';

import './index.html';
import './style.scss';

import * as dat from 'dat.gui';

let pointAnimation = document.getElementById('point-animation');
let svgContainer = document.getElementsByTagName('svg')[0];

let state = {
  pause: false,
  period: 5,
  angle: 0,
  x: 1,
  y: 0
};

const gui = new dat.GUI();
let pauseController = gui.add(state, 'pause').listen();
let periodController = gui.add(state, 'period', 1, 9, 1);
let angleController = gui.add(state, 'angle', 0, 1, .0001).listen();
let xController = gui.add(state, 'x', -1, 1, .0001).listen();
let yController = gui.add(state, 'y', -1, 1, .0001).listen();

let updateAnimation = () => {
  pointAnimation.setAttribute('dur', state.period + 's');
  svgContainer.setCurrentTime(state.angle * state.period);
};
updateAnimation();

pauseController.onChange((val) => {
  if (val) {
    svgContainer.pauseAnimations();
  } else {
    svgContainer.unpauseAnimations();
  }
});

periodController.onChange((val) => {
  updateAnimation();
});

angleController.onChange((val) => {
  svgContainer.pauseAnimations();
  updateAnimation();
});

angleController.onFinishChange((val) => {
  if (!state.pause) {
    svgContainer.unpauseAnimations();
  }
});

let updateAngle = () => {
  if (!state.pause) {
    let angle = (svgContainer.getCurrentTime() % state.period) / state.period;
    state.angle = angle;
  }
};

let yPoint = document.getElementById('y-point');
let xPoint = document.getElementById('x-point');
let update = () => {
  let x, y;
  y = -0.25 * Math.sin(state.angle * 2 * Math.PI);
  yPoint.setAttribute('cy', y);

  x = 0.25 * Math.cos(state.angle * 2 * Math.PI);
  xPoint.setAttribute('cx', x);

  let angle = (svgContainer.getCurrentTime() % state.period) / state.period;
  state.angle = angle;
  state.y = y*-4;
  state.x = x*4;

  requestAnimationFrame(update);
};
update();
