const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const OUTDIR = path.resolve(__dirname, 'public');

module.exports = {
  mode: 'development',
  devtool: 'inline-source-map',

  entry: './src/index.js',
  output: {
    filename: 'bundle.js',
    path: OUTDIR,
  },

  // cleans OUTDIR
  plugins: [
    new CleanWebpackPlugin()
  ],

  module: {
    rules: [

      // index html
      {
        test: /index\.html/,
        loader: 'file-loader',
        options: {
          name: 'index.html'
        }
      },

      // css
      {
        test: /\.s[ac]ss$/i,
        use: [
          'style-loader',
          'css-loader',
          'sass-loader'
        ]
      }
    ]
  },

  // dev server
  devServer: {
    contentBase: OUTDIR,
    compress: true,
    port: 9000
  }
};
